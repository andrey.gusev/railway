###route
class Route
  include InstanceCounter

  attr_reader :name, :stations

  @@routes = []

  def initialize(name, from_station, to_station)
    @stations = [from_station, to_station]
    @name = name.to_s
    validate!
    @@routes << self
    register_instance
  end

  def self.all
    @@routes
  end

  def stations_list
    stations.map(&:name).join(' -> ')
  end

  def add_transit_station(index, station)
    if index.zero? || index > @stations.length
      raise ParamError, ERRORS[:station_index]
    else
      @stations.insert(index, station)
    end
  end

  def del_transit_station(station)
    if stations.index(station).zero? || stations.index(station) == stations.size - 1
      raise ParamError, ERRORS[:cant_delete_station]
    else
      stations.delete(station)
      puts ">>> Обновленный маршрут: #{stations_list.to_s.chomp}"
    end
  end

  protected

  ROUTE_RGX = /^[a-z]+[0-9]*$/i.freeze

  def validate!
    raise ParamError, ERRORS[:route_name] if name !~ ROUTE_RGX

  end

end
