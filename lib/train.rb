class Train
  include Maker
  include Validate
  include ErrMsg

  attr_reader :speed, :route, :number, :wagons, :location_index

  @@trains = []

  def initialize(number)
    raise NotImplementedError if instance_of?(Train)

    @number = number.to_s
    validate!
    @wagons = []
    @speed = 0
    @route = nil
    @location_index = nil
    @@trains << self
  end

  def self.all
    @@trains
  end

  def self.find(number)
    all.find do |train|
      train.number == number.to_s
    end
  end

  def info
    puts "number: #{number}"
    puts "type: #{self.class}"
    puts "wagons: #{wagons if wagons}"
    puts "route: #{route.stations_list unless route.nil?}"
    puts "speed: #{speed}"
  end

  def add_wagon(wagon)
    raise ParamError, ERRORS[:speed] if @speed.nonzero?
    raise ParamError, ERRORS[:duplicate_wagon] if wagon.taken

    wagon.taken = true
    @wagons << wagon
  end

  def remove_wagon(wagon)
    raise ParamError, ERRORS[:speed] if @speed.nonzero?
    raise ParamError, ERRORS[:train_is_empty] if @wagons.empty?

    wagon.taken = false
    @wagons.delete(wagon)
  end

  def accel(speed_inc)
    raise ParamError, ERRORS[:wrong_speed] if speed_inc.negative?

    @speed += speed_inc
  end

  def brake
    raise ParamError, ERRORS[:train_is_still] if speed.zero?

    @speed = 0
  end

  def assign_route(route)
    @route = route
    @location_index = 0
    current_station.take_a_train(self)
  end

  def go_next_station
    raise ParamError, ERRORS[:end_station] if @location_index == route.stations.length - 1

    current_station.send_a_train(self)
    next_station.take_a_train(self)
    @location_index += 1
  end

  def go_prev_station
    raise ParamError, ERRORS[:start_station] if @location_index.zero?

    current_station.send_a_train(self)
    prev_station.take_a_train(self)
    @location_index -= 1
  end

  def current_station
    raise ParamError, ERRORS[:no_train_route] if route.nil?

    route.stations[@location_index]
  end

  def next_station
    raise ParamError, ERRORS[:end_station] if @location_index == route.stations.length - 1

    route.stations[@location_index + 1]
  end

  def prev_station
    raise ParamError, ERRORS[:start_station] if @location_index.zero?

    route.stations[@location_index - 1]
  end

  protected

  TRAIN_RGX = /^([а-я]{3}|[a-z]{3}|[0-9]{3})-?([а-я]{2}|[a-z]{2}|[0-9]{2})$/i.freeze

  def validate!
    raise ParamError, ERRORS[:train_number] if number !~ TRAIN_RGX

  end

end
