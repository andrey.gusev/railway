###station
class Station
  include InstanceCounter
  include Validate

  attr_reader :trains, :name

  @@stations = []

  def initialize(name)
    @name = name.to_s
    validate!
    @trains = []
    @@stations << self
    register_instance
  end

  def self.all
    @@stations
  end

  def trains_by_type(train_type)
    trains.select { |train| train.instance_of?(train_type) }
  end

  def take_a_train(train)
    @trains << train
  end

  def send_a_train(train)
    @trains.delete(train)
  end

  protected

  STATION_RGX = /^[a-z]+$/i.freeze
  
  def validate!
    raise ParamError, ERRORS[:station_name] if name !~ STATION_RGX

  end

end
