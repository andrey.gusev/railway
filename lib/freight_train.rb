class FreightTrain < Train
  include InstanceCounter

  def initialize(number)
    super
    register_instance
  end

  def add_wagon(wagon)
    err_msg_wrong_cat = 'К этому поезду можно добавлять только грузовые вагоны'
    raise StandardError, err_msg_wrong_cat unless wagon.is_a?(FreightWagon)

    super
  end

end
