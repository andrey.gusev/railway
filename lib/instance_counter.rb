module InstanceCounter
  def self.included(inc_class)
    inc_class.extend ClassMethods
    inc_class.include InstanceMethods
  end

  module ClassMethods
    # * Методы класса:
    # - instances, который возвращает кол-во экземпляров данного класса
    attr_accessor :instances

    def add_instance
      count_class = self
      count_class.instances = count_class.instances.to_i + 1
    end
  end

  module InstanceMethods
    # Инстанс-методы:
    # - register_instance, который увеличивает счетчик кол-ва
    # экземпляров класса и который можно вызвать из конструктора.
    # При этом данный метод не должен быть публичным.

    protected

    def register_instance
      self.class.add_instance
    end
  end
end
