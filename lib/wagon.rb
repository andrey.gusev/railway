class Wagon
  include Maker
  include Validate

  attr_reader :number
  attr_accessor :taken

  @@wagons = []

  def initialize(number)
    raise NotImplementedError if instance_of?(Wagon)

    @number = number.to_s
    validate!
    @taken = false
    @@wagons << self
  end

  def self.all
    @@wagons
  end

  protected

  WAGON_RGX = /^[a-z]+\d+$/i.freeze

  def validate!
    raise ParamError, ERRORS[:wagon_number] if number !~ WAGON_RGX

  end

end
