module Validate

  def valid?
    validate!
    true
  rescue ParamError
    false
  end

end
