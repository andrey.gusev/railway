class Menu
  include ErrMsg

  def top_level_menu
    puts 'Управление поездами: 1'
    puts 'Управление станциями: 2'
    puts 'Управление маршрутами: 3'
    puts 'Управление вагонами: 4'
    puts 'Завершить работу: 0'
    result = gets.to_i

    case result
    when 1
      trains_menu
    when 2
      stations_menu
    when 3
      routes_menu
    when 4
      wagons_menu
    when 0
      exxxit
    end
  end

  ### trains
  def trains_menu
    puts 'Создать грузовой поезд: 1'
    puts 'Создать пассажирский поезд: 2'
    puts 'Список всех поездов: 3'
    puts 'Присвоить поезду маршрут: 4'
    puts 'Добавить вагон: 5'
    puts 'Удалить вагон: 6'
    puts 'Местоположение поезда: 7'
    puts 'Добавить скорость: 8'
    puts 'Остановить поезд: 9'
    puts 'Узнать следующую станцию: 10'
    puts 'Отправить поезд на следующую станцию: 11'
    puts 'Узнать предыдущую станцию: 12'
    puts 'Отправить поезд на предыдущую станцию: 13'
    puts 'Главное меню: 0'
    result = gets.to_i

    case result
    when 1
      create_train(:freight)
    when 2
      create_train(:passenger)
    when 3
      all_trains
    when 4
      assign_route
    when 5
      add_wagon
    when 6
      remove_wagon
    when 7
      location
    when 8
      accelerate
    when 9
      braking
    when 10
      next_station
    when 11
      go_next_station
    when 12
      prev_station
    when 13
      go_prev_station
    when 0
      top_level_menu
    else trains_menu
    end
  end

  def create_train(category)
    case category
    when :freight
      puts '*** Создать грузовой поезд'
      puts '??? Задайте номер поезда: '
      begin
        train = FreightTrain.new(gets.to_s.chomp)
        puts "Поезд создан: #{train}"
        puts '>>> ----------'
      rescue ParamError => e
        puts e.message.to_s
        create_train(:freight)
      end

      trains_menu
    when :passenger
      puts '*** Создать пассажирский поезд'
      puts '??? Задайте номер поезда: '
      begin
        train = PassengerTrain.new(gets.to_s.chomp)
        puts ">>> Поезд создан: #{train}"
        puts '>>> ----------'
      rescue ParamError => e
        puts e.message.to_s
        create_train(:passenger)
      end

      trains_menu
    end
  end

  def all_trains
    begin
      raise ParamError, ERRORS[:no_trains] if Train.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    else
      Train.all.each do |train|
        puts '>>> Train info'
        train.info
        puts '>>> ----------'
      end
    end

    trains_menu
  end

  def assign_route
    raise ParamError, ERRORS[:no_trains] if Train.all.empty?
    raise ParamError, ERRORS[:no_trains] if Route.all.empty?
  rescue ParamError => e
    puts e.message.to_s
    trains_menu
  else
    puts '*** Присвоить маршрут поезду'
    puts '??? Поезд (выберите номер): '
    Train.all.each.with_index do |train, index|
      puts "#{train.number.to_s.chomp} - #{index}"
    end
    train = Train.all.fetch(gets.to_i, ERRORS[:no_such_train])

    puts '??? Маршрут (выберите номер): '
    Route.all.each.with_index do |route, index|
      puts "#{route.name.to_s.chomp} (#{route.stations_list.to_s.chomp}) - #{index}"
    end
    route = Route.all.fetch(gets.to_i, ERRORS[:no_such_route])

    train.assign_route(route)
    puts ">>> Поезд #{train.number} отправлен по маршруту [#{route.stations_list}]"
    puts '>>> ----------'
    trains_menu
  end

  def add_wagon
    begin
      raise ParamError, ERRORS[:no_wagons] if Wagon.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      wagons_menu
    end

    begin
      raise ParamError, ERRORS[:no_trains] if Train.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    end

    puts '*** Добавить вагон к поезду'
    puts '??? Поезд (выберите номер): '
    Train.all.each.with_index do |train, index|
      puts "#{train.number.to_s.chomp} - #{index}"
    end
    train = Train.all.fetch(gets.to_i, ERRORS[:no_such_train])

    puts '??? Вагон (выберите номер): '
    Wagon.all.each.with_index do |wagon, index|
      puts "#{wagon.number.to_s.chomp} - #{index}"
    end
    wagon = Wagon.all.fetch(gets.to_i, ERRORS[:no_such_wagon])

    begin
      train.add_wagon(wagon)
    rescue ParamError => e
      puts e.message.to_s
    end
    trains_menu
  end

  def remove_wagon
    begin
      raise ParamError, ERRORS[:no_wagons] if Wagon.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      wagons_menu
    end

    begin
      raise ParamError, ERRORS[:no_trains] if Train.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    end

    puts '*** Удалить вагон из поезда'
    puts '??? Поезд (выберите номер): '
    Train.all.each.with_index do |train, index|
      puts "#{train.number.to_s.chomp} - #{index}"
    end
    train = Train.all.fetch(gets.to_i, ERRORS[:no_such_train])

    puts '??? Вагон (выберите номер): '
    Wagon.all.each.with_index do |wagon, index|
      puts "#{wagon.number.to_s.chomp} - #{index}"
    end
    wagon = Wagon.all.fetch(gets.to_i, ERRORS[:no_such_wagon])

    begin
      train.remove_wagon(wagon)
    rescue ParamError => e
      puts e.message.to_s
    end

    trains_menu
  end

  def location
    begin
      raise ParamError, ERRORS[:no_trains] if Train.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    end

    puts '??? Поезд (выберите номер): '
    Train.all.each.with_index do |train, index|
      puts "#{train.number.to_s.chomp} - #{index}"
    end

    begin
      train = Train.all.fetch(gets.to_i)
      puts ">>> Поезд на станции #{train.route.stations.fetch(train.location_index).name}"
      puts ">>> Маршрут #{train.route.stations_list.to_s.chomp}"
      puts '>>> ----------'
    rescue ParamError => e
      puts e.message.to_s
    end

    trains_menu
  end

  def next_station
    begin
      raise ParamError, ERRORS[:no_trains] if Train.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    end

    begin
      raise ParamError, ERRORS[:no_train_route] if Train.route.nil?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    end

    puts '??? Поезд (выберите номер): '
    Train.all.each.with_index do |train, index|
      puts "#{train.number.to_s.chomp} - #{index}"
    end
    train = Train.all.fetch(gets.to_i)

    begin
      train.next_station
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    else
      puts "Следующая станция: #{train.route.stations.fetch(train.next_station).name}"
    end

    trains_menu
  end

  def go_next_station
    begin
      raise ParamError, ERRORS[:no_trains] if Train.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    end

    puts '??? Поезд (выберите номер): '
    Train.all.each.with_index do |train, index|
      puts "#{train.number.to_s.chomp} - #{index}"
    end
    train = Train.all.fetch(gets.to_i)

    begin
      train.go_next_station
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    else
      puts ">>> Поезд отправлен на станцию #{train.route.stations.fetch(train.location_index).name}"
      puts '>>> ----------'
    end

    trains_menu
  end

  def prev_station
    begin
      raise ParamError, ERRORS[:no_trains] if Train.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    end

    begin
      raise ParamError, ERRORS[:no_train_route] if Train.route.nil?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    end

    puts '??? Поезд (выберите номер): '
    Train.all.each.with_index do |train, index|
      puts "#{train.number.to_s.chomp} - #{index}"
    end
    train = Train.all.fetch(gets.to_i)

    begin
      train.prev_station
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    else
      puts "Следующая станция: #{train.route.stations.fetch(train.prev_station).name}"
    end

    trains_menu
  end

  def go_prev_station
    begin
      raise ParamError, ERRORS[:no_trains] if Train.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    end

    puts '??? Поезд (выберите номер): '
    Train.all.each.with_index do |train, index|
      puts "#{train.number.to_s.chomp} - #{index}"
    end
    train = Train.all.fetch(gets.to_i)

    begin
      train.go_prev_station
    rescue ParamError => e
      puts e.message.to_s
    else
      puts ">>> Поезд отправлен на станцию #{train.route.stations.fetch(train.location_index).name}"
      puts '>>> ----------'
    end

    trains_menu
  end

  def accelerate
    begin
      raise ParamError, ERRORS[:no_trains] if Train.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    end

    puts '??? Поезд (выберите номер): '
    Train.all.each.with_index do |train, index|
      puts "#{train.number.to_s.chomp} - #{index}"
    end
    train = Train.all.fetch(gets.to_i, ERRORS[:no_such_train])

    puts '??? Приращение скорости: '
    speed = gets.to_i

    begin
      train.accel(speed)
    rescue ParamError => e
      puts e.message.to_s
    end

    trains_menu
  end

  def braking
    begin
      raise ParamError, ERRORS[:no_trains] if Train.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      trains_menu
    end

    puts '??? Поезд (выберите номер): '
    Train.all.each.with_index do |train, index|
      puts "#{train.number.to_s.chomp} - #{index}"
    end

    train = Train.all.fetch(gets.to_i, ERRORS[:no_such_train])

    begin
      train.brake
    rescue ParamError => e
      puts e.message.to_s
    end

    trains_menu
  end

  ### routes
  def routes_menu
    puts 'Создать маршрут: 1'
    puts 'Список маршрутов: 2'
    puts 'Добавить станцию в маршрут: 3'
    puts 'Удалить станцию из маршрута: 4'
    puts 'Главное меню: 0'
    result = gets.to_i

    case result
    when 1
      create_route
    when 2
      routes_list
    when 3
      add_station_to_route
    when 4
      delete_station_from_route
    when 0
      top_level_menu
    else routes_menu
    end
  end

  def create_route
    begin
      raise ParamError, ERRORS[:no_stations] if Station.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      stations_menu
    end

    begin
      raise ParamError, ERRORS[:only_one_station] if Station.all.length < 2
    rescue ParamError
      stations_menu
    end

    puts '*** Создать маршрут'
    print '??? Имя маршрута: '
    name = gets.to_s.chomp
    puts '??? Начальная станция (номер): '
    Station.all.each.with_index do |station, index|
      puts "#{station.name.to_s.chomp} - #{index}"
    end
    route_begin = Station.all.fetch(gets.to_i, ERRORS[:no_such_station])

    puts '??? Конечная станция (номер): '
    Station.all.each.with_index do |station, index|
      puts "#{station.name.to_s.chomp} - #{index}"
    end
    route_end = Station.all.fetch(gets.to_i, ERRORS[:no_such_station])

    begin
      route = Route.new(name, route_begin, route_end)
    rescue ParamError => e
      puts e.message.to_s
      create_route
    else
      puts ">>> Маршрут создан: [#{route.stations_list.to_s.chomp}]"
      puts '>>> ----------'
    end

    routes_menu
  end

  def routes_list
    begin
      raise ParamError, ERRORS[:no_routes] if Route.all.length.zero?
    rescue ParamError => e
      puts e.message.to_s
      routes_menu
    else
      puts '*** Существующие маршруты:'
      Route.all.each do |route|
        puts ">>> #{route.name.to_s.chomp}: [#{route.stations_list}]"
      end
      puts '>>> ----------'
    end

    routes_menu
  end

  def add_station_to_route
    begin
      raise ParamError, ERRORS[:no_stations] if Station.all.empty?
    rescue ParamError => e
      pits e.message.to_s
      stations_menu
    end

    begin
      raise ParamError, ERRORS[:no_routes] if Route.all.empty?
    rescue ParamError => e
      puts e.message.to_s
      routes_menu
    end

    puts '??? Выберите маршрут: '
    Route.all.each.with_index do |route, index|
      puts "#{route.name.to_s.chomp} - #{index}"
    end
    route = Route.all.fetch(gets.to_i, ERRORS[:no_such_route])

    puts '??? Выберите станцию: '
    Station.all.each.with_index do |station, index|
      puts "#{station.name.to_s.chomp} - #{index}"
    end
    station = Station.all.fetch(gets.to_i, ERRORS[:no_such_station])

    puts '??? Выберите позицию в маршруте'
    puts '??? (Первая станция = 0)'
    position = gets.to_i

    begin
      route.add_transit_station(position, station)
    rescue ParamError => e
      puts e.message.to_s
    end

    routes_menu
  end

  def delete_station_from_route
    begin
      raise ParamError, ERRORS[:no_stations] if Station.all.empty?
    rescue ParamError => e
      puts e.message.to_s

      stations_menu
    end

    begin
      raise ParamError, ERRORS[:no_routes] if Route.all.empty?
    rescue ParamError => e
      puts e.message.to_s

      routes_menu
    end

    puts '??? Выберите маршрут: '
    Route.all.each.with_index do |route, index|
      puts "#{route.name.to_s.chomp} - #{index}"
    end
    route = Route.all.fetch(gets.to_i, ERRORS[:no_such_route])

    puts '??? Выберите станцию в маршруте: '
    route.stations.each.with_index do |station, index|
      puts "#{station.name.to_s.chomp} - #{index}"
    end
    station = route.stations.fetch(gets.to_i, ERRORS[:no_such_station])

    begin
      route.del_transit_station(station)
    rescue ParamError => e
      puts e.message.to_s
    end

    routes_menu
  end

  ### stations
  def stations_menu
    puts 'Создать станцию: 1'
    puts 'Список станций: 2'
    puts 'Список всех поездов на станции: 3'
    puts 'Список поездов на станции по типу: 4'
    puts 'Главное меню: 0'

    result = gets.to_i

    case result
    when 1
      create_station
    when 2
      stations_list
    when 3
      trains_list
    when 4
      trains_by_type
    when 0
      top_level_menu
    else stations_menu
    end
  end

  def create_station
    puts '*** Создать станцию'
    print '??? Задайте имя (только английские буквы): '
    begin
      station = Station.new(gets.to_s.chomp)
    rescue ParamError => e
      puts e.message.to_s
    else
      puts ">>> Станция создана: #{station}"
      puts '>>> ----------'
    end

    stations_menu
  end

  def stations_list
    begin
      raise ParamError, ERRORS[:no_stations] if Station.all.empty?
    rescue ParamError => e
      puts e.message.to_s
    else
      puts '*** Список станций:'
      Station.all.each do |station|
        puts ">>> #{station.name}"
      end
    end

    stations_menu
  end

  def trains_list
    puts '??? Выберите станцию: '
    Station.all.each.with_index do |station, index|
      puts "#{station.name.to_s.chomp} - #{index.to_s.chomp}"
    end

    station = Station.all.fetch(gets.to_i, 'out of range')

    if station.trains.empty?
      puts ">>> На станции #{station.name} поездов нет."
    else
      puts ">>> #{station.trains}"
    end

    stations_menu
  end

  def trains_by_type
    puts '??? Выберите станцию: '
    Station.all.each.with_index do |station, index|
      puts "#{station.name.to_s.chomp} - #{index.to_s.chomp}"
    end

    station = Station.all.fetch(gets.to_i, 'out of range')

    if station.trains.empty?
      puts ">>> На станции #{station.name} поездов нет."
      stations_menu
    end

    puts '??? Тип поезда: '
    puts '??? FreightTrain - 0 '
    puts '??? PassengerTrain - 1 '
    result = gets.to_i

    case result
    when 0
      puts ">>> #{station.trains_by_type(FreightTrain)}"
      stations_menu
    when 1
      puts ">>> #{station.trains_by_type(PassengerTrain)}"
      stations_menu
    end

    stations_menu
  end

  ### wagons
  def wagons_menu
    puts 'Создать грузовой вагон: 1'
    puts 'Создать пассажирский вагон: 2'
    puts 'Список всех вагонов: 3'
    puts 'Главное меню: 0'
    result = gets.to_i

    case result
    when 1
      create_wagon(:freight)
    when 2
      create_wagon(:passenger)
    when 3
      wagons_list
    when 0
      top_level_menu
    else wagons_menu
    end
  end

  def create_wagon(category)
    case category
    when :freight
      puts '*** Создать грузовой вагон'
      puts '??? Задайте номер вагона (только англ. буквы и цифры): '
      begin
        wagon = FreightWagon.new(gets.to_s.chomp)
      rescue ParamError => e
        puts e.message.to_s
      else
        puts ">>> Вагон создан: #{wagon}"
        puts '>>> ----------'
      end

      wagons_menu
    when :passenger
      puts '*** Создать пассажирский вагон'
      puts '??? Задайте номер вагона (только англ. буквы и цифры): '
      begin
        wagon = PassengerWagon.new(gets.to_s.chomp)
      rescue ParamError => e
        puts e.message.to_s
      else
        puts ">>> Вагон создан: #{wagon}"
        puts '>>> ----------'
      end

      wagons_menu
    end
  end

  def wagons_list
    begin
      raise ParamError, ERRORS[:no_wagons] if Wagon.all.empty?
    rescue ParamError => e
      puts e.message.to_s
    else
      Wagon.all.each do |wagon|
        puts ">>> #{wagon.number.to_s.chomp}: #{wagon.class}"
      end
      puts '>>> ----------'
    end

    wagons_menu
  end

  def exxxit
    system('clear')
    exit
  end
end
