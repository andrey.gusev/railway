require_relative 'lib/error_messages'
require_relative 'lib/validate'
require_relative 'lib/param_error'
require_relative 'lib/instance_counter'
require_relative 'lib/maker'
require_relative 'lib/train'
require_relative 'lib/freight_train'
require_relative 'lib/passenger_train'
require_relative 'lib/wagon'
require_relative 'lib/freight_wagon'
require_relative 'lib/passenger_wagon'
require_relative 'lib/route'
require_relative 'lib/station'
require_relative 'ui/menu'
require 'byebug'

class Main
  def initialize
    @menu = Menu.new
  end

  def call
    @menu.top_level_menu
  end

end

main = Main.new

main.call
